#!/usr/bin/env bash
rm -rf exercise
mkdir exercise

cd exercise

git init


for i in {1..10}
do
	GOODNESS=$([ "$i" = "8" ] && echo "bad" || echo "good")
    echo "$i-$GOODNESS" > $i-$GOODNESS.txt
    git add $i-$GOODNESS.txt

    git commit -m "$i-$GOODNESS"
done

